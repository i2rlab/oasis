# OASIS+: Machine Learning Based OASIS Severity Score Calculator #

This repo includes the code for applying OASIS+ severity score [1] to a test set.
An online calculator for OASIS+ score is also available at https://oasis-score.herokuapp.com/


### References ###
[1] OASIS+: leveraging machine learning to improve the prognostic accuracy of OASIS severity score for predicting in-hospital mortality. To be submitted.
