joblib==0.13.2
pandas==0.25.0
matplotlib==3.1.0
streamlit==0.58.0
numpy==1.18.1
Pillow==8.0.1
scikit_learn==0.23.2
xgboost==0.80